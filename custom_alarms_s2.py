import tango
import os


tango_host = os.environ.get("TANGO_HOST", "tango-databaseds:10000")


# add extractor alarms
def create_formula(formula_device: str, attr_formula: str):
    return formula_device + f"/{attr_formula}"

fqdn = [
    "mid-csp/control/0",
    "mid-csp/control/0",
    "mid-csp/subarray/01",
    "mid-csp/subarray/01",
    "mid-csp/subarray/01",
    "mid-csp/subarray/01",
    "mid-dish/dish-manager/SKA001",
    "mid-dish/dish-manager/SKA001",
    "mid-dish/dish-manager/SKA001",
    "mid-dish/dish-manager/SKA001",
    "mid-dish/dish-manager/SKA001",
    "mid-dish/simulator-spfc/SKA001",
    "mid-dish/simulator-spfc/SKA001",
    "mid-dish/simulator-spfc/SKA001",
    "mid-dish/simulator-spfc/SKA001",
    "mid-dish/simulator-spfc/SKA001",
    "mid-sdp/control/0",
    "mid-sdp/subarray/01",
    "mid_csp_cbf/sub_elt/controller",
    "mid_csp_cbf/talon_board/001",
    "mid_csp_cbf/talon_board/001",
    "mid_csp_cbf/talon_board/001",
    "mid_csp_cbf/talon_board/001",
    "mid_csp_cbf/talon_board/001",
    "mid_csp_cbf/talon_board/001",
    "ska001/spfrxpu/controller",
    "ska001/spfrxpu/controller",
    "ska001/spfrxpu/controller",
    "ska001/spfrxpu/controller",
    "ska001/spfrxpu/controller",
    "ska001/spfrxpu/controller",
    "ska_mid/tm_central/central_node",
    "ska_mid/tm_central/central_node"
]

alarm_names = [
    "CSP_CONTROLLER_simMode",
    "CSP_CONTROLLER_State",
    "CSP_SUBARRAY_healthState",
    "CSP_SUBARRAY_isCommunicating",
    "CSP_SUBARRAY_obsState",
    "CSP_SUBARRAY_simMode",
    "DISH_MANAGER_achievedTargetLock",
    "DISH_MANAGER_Capturing",
    "DISH_MANAGER_configuredBand",
    "DISH_MANAGER_pointingstate",
    "DISH_MANAGER_spfrxConnection",
    "SPFC_B1CababilityState",
    "SPFC_bandInFocus",
    "SPFC_healthState",
    "SPFC_operatingMode",
    "SPFC_powerState",
    "SDP_CONTROL_healthState",
    "SDP_SUBARRAY_obsState",
    "CBF_CONTROLLER_State",
    "T1_DIMMTemperatures",
    "T1_fansFault",
    "T1_FPGADieTemperature",
    "T1_humiditySensorTemperature",
    "T1_LtmTemperatureWarning",
    "T1_LtmVoltageWarning",
    "SPFRX_CONTROLLER_B1CapabilityState",
    "SPFRX_CONTROLLER_configuredBand",
    "SPFRX_CONTROLLER_capturingData",
    "SPFRX_CONTROLLER_engTemperatureValue",
    "SPFRX_CONTROLLER_kValue",
    "SPFRX_CONTROLLER_RXPUHumidity",
    "TMC_CENTRAL_NODE_isDishVccConfigSet",
    "TMC_CENTRAL_NODE_telescopeHealthState"
]

attribute_formulas = ["simulationMode == 0",
                      "State == 9",
                      "healthState != 0",
                      "isCommunicating != 1",
                      "obsState == 5",
                      "simulationMode == 0",
                      "achievedTargetLock == 1",
                      "capturing == 1",
                      "configuredBand == 1",
                      "pointingState == 3",
                      "spfrxConnectionState == 2",
                      "b1CapabilityState == 3",
                      "bandInFocus == 1",
                      "healthState != 0",
                      "operatingMode == 3",
                      "powerState == 2",
                      "healthState != 0",
                      "obsState == 5",
                      "healthState != 0",
                      "DIMMTemperatures > 25.0",
                      "FansFault == 1",
                      "FpgaDieTemperature > 25.0",
                      "HumiditySensorTemperature > 25.0",
                      "LtmTemperatureWarning == 1",
                      "LtmVoltageWarning == 1",
                      "b1CapabilityState == 4",
                      "configuredBand == 1",
                      "capturingData == 1",
                      "engTemperatureValues[0] > 25.0",
                      "kValue != 1",
                      "rxpuHumidity > 25.0",
                      "isDishVccConfigSet == 1",
                      "telescopeHealthState != 0"
]

descriptions = [
    "The CSP Controller is now out of simulationMode",
    "The CSP Controller is in FAULT state",
    "The CSP Subarray is NOT healthy",
    "The CSP Subarray is communicating",
    "The CSP Subarray is SCANNING",
    "The CSP Subarray is now out of simulationMode",
    "The dishManager has achieved targetLock",
    "The dishManager is currently capturing",
    "The current configuredBand of the dishManager is B1",
    "The current pointingState of the dishManager is SCAN",
    "The dishManager is connected to the SPFRx",
    "The SPFC is in full operation mode on B1",
    "The current band in focus for the SPFC is B1",
    "The SPFC is NOT healthy",
    "The SPFC is operational",
    "The SPFC is in FULL_POWER mode",
    "The SDP Controller is NOT healthy",
    "The SDP Subarray is SCANNING",
    "The CBF Controller is NOT healthy",
    "DIMM Temps Above 25 for T1",
    "T1 Fan FAULT",
    "T1 Die Temp Above 25",
    "T1 Humidity Above 25",
    "T1 LTM Temp Warning",
    "T1 LTM Voltage Warning",
    "The SPFRx Controller is operating on B1",
    "The SPFRx Controller's configured band is B1",
    "The SPFRx Controller is capturing data",
    "The RXPU Enclosure Temperature is above 25",
    "The SPFRx configured kValue is not 1",
    "The RXPU Humidity is above 25",
    "DishVccConfig has been set",
    "Telescope HealthState has left OK"
]

severities = [
    "INFO",
    "WARNING",
    "WARNING",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "INFO",
    "WARNING",
    "INFO",
    "INFO",
    "WARNING",
    "INFO",
    "WARNING",
    "WARNING",
    "WARNING",
    "WARNING",
    "WARNING",
    "WARNING",
    "WARNING",
    "INFO",
    "INFO",
    "INFO",
    "WARNING",
    "INFO",
    "WARNING",
    "INFO",
    "WARNING"
]

dp = tango.DeviceProxy(f"{tango_host}/sys/alarm/pyalarm01")
config = dict()
config["AlarmList"] = list()
config["AlarmDescriptions"] = list()
config["AlarmSeverities"] = list()
config["AlarmReceivers"] = list()
for i, _ in enumerate(attribute_formulas):
    config["AlarmList"].append(f"{alarm_names[i]}: {create_formula(fqdn[i], attribute_formulas[i])}")
    config["AlarmDescriptions"].append(f"{alarm_names[i]}: {descriptions[i]}")
    config["AlarmSeverities"].append(f"{alarm_names[i]}: {severities[i]}")
    config["AlarmReceivers"].append(f"{alarm_names[i]}:")
    
dp.put_property(config)
dp.command_inout("Init")