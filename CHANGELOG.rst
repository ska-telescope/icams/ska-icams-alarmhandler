############
Change Log
############

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

2024-11-19
***********
* MAP-156: Remove pyalarm and achtung completely from charts
           Add tango-gql to charts
           Tweak frontend and backend charts to support tango-gql
