# Helm chart for IC@MS frontend

## Config

### General

| Name             | Description                         | Default value        |
| ---------------- | ----------------------------------- | -------------------- |
| config.icams_api | The hostname of the backend service | http://backend3:3010 |

### Custom path

With this setting you can access IC@MS using custom path. For example your app is
hosted under `https://domain.com/` but you want to access it with `https://domain.com/namespace/icams/`.
Then all you need to do is set
`config.customPath.prefixPath` to `/namespace/icams` and change path in Ingress host
to the same as your `prefixPath`. If you want to access admin panel you have change
this path in backend Helm chart too. It should be able to configure this path the same
way we do here, but to be sure, check backend chart README.

By default, you can access app under `/`

| Name                         | Description                        | Default value |
| ---------------------------- | ---------------------------------- | ------------- |
| config.customPath.prefixPath | Custom path. Should start with `/` | ""            |
