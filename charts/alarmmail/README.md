# Alarm Mail

## Additional Information on Logic:

- **Alarm Handler**: Device server responsible for listening to specific formulas and assigning specific alarm states.

- **Alarm Notify**: Device server that listens to alarm handlers' for changes and if it detects a change of state in an alarm according to the configuration, it executes a command on a specified DS which can be for eg. email sending via device server **Alarm Mail**. It can basicly execute any command on any device server provided, which means the usage can be extended to eg. SMS sending.

## Introduction

Project with description can be found at [this link](https://gitlab.elettra.eu/cs/ds/alarm-mail).

Before running Docker, you need to create a record in the database:

    tango_admin --add-server alarm-mail-srv/01 AlarmMail sys/alarm/alarm-mail-01
  

## Configuration and Brief Description:

Field descriptions for modifications in ICAMS:

- **Device Name**: Name of the Alarm Mail instance.

- **SMTP server address**: SMTP server address.

- **Email Name**: email_name

- **Email User**: email_user, (email_name <email_user>)


## Limitations

Due to a known problem on the pytango side (https://gitlab.com/tango-controls/pytango/-/issues/545), the AlarmMail device server does not work properly with icams, a restart is performed when changing and/or adding an instance, which causes an error and the server crashes, it must be manually restarted
