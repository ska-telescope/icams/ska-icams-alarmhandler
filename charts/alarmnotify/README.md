# Alarm Notify

To notify about the alarms from Alarm Handler device using e-mail two described below device servers must be used.

## Additional Information on Logic:

- **Alarm Handler**: Device server responsible for listening to specific formulas and assigning specific alarm states.

- **Alarm Notify**: Device server that listens to alarm handlers' for changes and if it detects a change of state in an alarm according to the configuration, it executes a command on a specified DS which can be for eg. email sending via device server **Alarm Mail**. It can basicly execute any command on any device server provided, which means the usage can be extended to eg. SMS sending.

## Introduction

Original project code with readme can be found at [this link](https://gitlab.elettra.eu/cs/ds/alarm-notify).

Before running Docker, you need to create a record in the database:

    tango_admin --add-server alarm-notify-srv/01 AlarmNotify sys/alarm/alarm-notify-01


## Configuration and Brief Description:

Field descriptions for modification in ICAMS:

- **Device Name**: Name of the Alarm Notify instance.

- **Notification Device**: Name of the device server used for sending messages (e.g., AlarmMail).

- **Notification Command**: Name of the command in Notification Device responsible for sending messages.

- **Subscribe Period**: Period in seconds of thread subscribing (and retrying subscription) for events.

- **Alarm Handler List**: List of alarm handlers to monitor in the form of */*/*,*/*/*,...

- **Groups Entry**: For alarms in group x and on the "state" event, it sends a message to the mail Receivers.

- **Add Receivers Entry**: For specifically defined "state", it sends an email to the address specified for receivers.

