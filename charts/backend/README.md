# Helm chart for IC@MS backend

## General Config

| Name                     | Description                         | Default value         |
| ------------------------ | ----------------------------------- | --------------------- |
| config.tango_host        | TANGO_HOST for whole app            | tango:10000           |
| config.mongo_db_host     | Hostname for MongoDB server         | icams-backend-mongodb |
| config.mongo_db_port     | Port for MongoDB server             | 27017                 |
| config.mongo_db_name     | Name of the MongoDB database        | icams                 |
| config.mongo_db_user     | Username for MongoDB authentication | icams                 |
| config.mongo_db_password | Password for MongoDB authentication | access4icams          |
| config.login_provider    | Change client login provider        | access4icams          |

## Device designation

Device designation is a feature that will prevent IC@MS from adding or modifying alarms on non-designated devices.
All alarms will still be listed, but you can only modify those on the designated devices. To enable this feature
you have to set `config.designation.enable` to `"True"`. With this option, IC@MS will check if appropriate property
is set on device server. This property is called `_Icams` and should be set to `editable`. If other value will be set,
IC@MS will treat it this device as immutable.

| Name                      | Description                                                                           | Default value |
| ------------------------- | ------------------------------------------------------------------------------------- | ------------- |
| config.designation.enable | Enable device designation. Set "True" if you want to enable it, otherwise put "False" | "False"       |

## MongoDB configuration

MongoDB is the only dependency of this Helm chart and is required for the app to work.

You can configure MongoDB under `mongodb` like this:

```yaml
mongodb:
  image:
    registry: docker.io
    repository: bitnami/mongodb
    tag: "6.0.10"
    pullPolicy: Always
    debug: true
```

## Mail config

| Name                            | Description                                                                                              | Default value                         |
| ------------------------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------- |
| config.mail.mail_server         | The hostname or IP address of the SMTP                                                                   | email-smtp.eu-central-1.amazonaws.com |
| config.mail.mail_port           | SMTP port                                                                                                | 587                                   |
| config.mail.mail_username       | Username or email address associated with the mail account. This also set SMTP_USER environment variable | ""                                    |
| config.mail.mail_password       | Password for the mail account. This also set SMTP_PASSWORD environment variable                          | ""                                    |
| config.mail.mail_use_tls        | Determines whether your application should use TLS                                                       | 1                                     |
| config.mail.mail_use_ssl        | Determines whether your application should use SSL                                                       | 0                                     |
| config.mail.mail_default_sender | Defines the default email address that will appear in the "From" field of outgoing email                 | michal.gandor@s2innovation.com        |

## Frontend config

| Name                | Description                   | Default value |
| ------------------- | ----------------------------- | ------------- |
| config.frontend.url | URL of the IC@MS frontend app | ""            |

## Custom path

Custom prefix to path for Flask admin panel. If we won't set this value, admin panel will be available
under the default path it is `/admin`.

_NOTE:_ It only works when we set the same path in frontend Helm chart.

| Name                         | Description                           | Default value |
| ---------------------------- | ------------------------------------- | ------------- |
| config.customPath.prefixPath | Prefix to path. Should start with `/` | ""            |
