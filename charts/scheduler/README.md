# Helm chart for IC@MS scheduler

## Config

| Name                         | Description                                     | Default value                          |
| ---------------------------- | ----------------------------------------------- | -------------------------------------- |
| config.tango_host            | TANGO_HOST for whole app                        | tango:10000                            |
| config.mongo_db_host         | Hostname for MongoDB server                     | icams-backend-mongodb                  |
| config.mongo_db_port         | Port for MongoDB server                         | 27017                                  |
| config.mongo_db_name         | Name of the MongoDB database                    | icams                                  |
| config.mongo_db_user         | Username for MongoDB authentication             | icams                                  |
| config.mongo_db_password     | Password for MongoDB authentication             | access4icams                           |
| config.snap_interval         | Interval of checking for changes in Snap database in seconds            | 300                                  |
| config.alarms_interval       | Interval of checking for changes in alarm devices in seconds            | 6                           |
