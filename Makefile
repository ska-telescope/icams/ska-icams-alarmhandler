#
# Project makefile for ska-mid-cbf-mcs project. 
PROJECT = ska-icams-alarmhandler

HELM_RELEASE ?= test##H ELM_RELEASE is the release that all Kubernetes resources will be labelled with

HELM_CHART ?= ska-icams-alarmhandler-umbrella## HELM_CHART the chart name

CI_REGISTRY ?= gitlab.com/ska-telescope/ska-mid-icams-alarmhandler

CI_PROJECT_DIR ?= .

# define private overrides for above variables in here
-include PrivateRules.mak
-include .make/helm.mk
include .make/release.mk
include .make/make.mk
include .make/helm.mk
include .make/docs.mk
include .make/base.mk
#
# Defines a default make target so that help is printed if make is called
# without a target
#
.DEFAULT_GOAL := help

help: ## show this help.
	@echo "make targets:"
	@echo "$(MAKEFILE_LIST)"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
